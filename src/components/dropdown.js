export default {
  data() {
    return {
      _contents: null,
      opening: false,
      closing: false,
      opened: false,
      closed: true,
    };
  },
  props: {
    align: {
      type: String,
      default: 'bottom right'
    },
    contentsWidth: {
      type: String,
      default: '200px'
    },
    animationDuration: {
      type: String,
      default: '150ms'
    }
  },
  mounted() {
    const root = this.$root.$el.querySelector('.dropdown-contents');

    this.$nextTick(() => {
      document.addEventListener('click', this.handleClick);
    });

    this._contents = root;
    this.setCssProperty(root, '--dropdown-animation-duration', this.animationDuration);
    this.setCssProperty(root, '--dropdown-contents-width', this.contentsWidth);
  },
  computed: {
    alignPropIsLeft() {
      return this.align.includes('left');
    },
    alignPropIsRight() {
      return this.align.includes('right');
    },
    alignPropIsBottom() {
      return this.align.includes('bottom');
    },
    alignPropIsTop() {
      return this.align.includes('top');
    },
    isOpened() {
      return this.opened;
    },
    isClosed() {
      return this.closed;
    },
    isOpening() {
      return this.opening;
    },
    isClosing() {
      return this.closing;
    },
  },
  methods: {
    handleClick(e) {
      if (this.isOpened
        && ! e.target.closest('.dropdown')) {
        this.close();
      }
    },
    toggle() {
      if (this.runing()) {
        return;
      }

      if (this.isOpened) {
        this.close();
      } else {
        this.open();
      }
    },
    runing() {
      return (this.opening || this.closing);
    },
    open() {
      this.opening = true;
      this.closed = false;
      this.opened = false;

      setTimeout(() => {
        this.opened = true;
        this.closed = ! this.opened;
        this.opening = false;
      }, this.animationDuration.replace(/\D/g,''));
    },
    close() {
      this.closing = true;
      this.closed = false;
      this.opened = false;

      setTimeout(() => {
        this.closing = false;
        this.opened = false;
        this.closed = ! this.opened;
      }, this.animationDuration.replace(/\D/g,''));
    },
    setCssProperty(element, key, value) {
      return element.style.setProperty(key, value);
    },
    getCssProperty(key) {
      return getComputedStyle(this.$root.$el).getPropertyValue(key);
    }
  }
}

